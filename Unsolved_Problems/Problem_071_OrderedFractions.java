package Unsolved_Problems;

import java.util.ArrayList;

import org.apache.commons.lang3.math.Fraction;

/*
* Consider the fraction, n/d, where n and d are positive integers. If n<d and HCF(n,d)=1, it is called a reduced proper fraction.
*
* If we list the set of reduced proper fractions for d ≤ 8 in ascending order of size, we get:
*
* 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
*
* It can be seen that 2/5 is the fraction immediately to the left of 3/7.
*
* By listing the set of reduced proper fractions for d ≤ 1,000,000 in ascending order of size, find the numerator of the fraction immediately to the left of 3/7.
*/

public class Problem_071_OrderedFractions {

    public static void main(String[] args)
    {
        int  limit = 10; // 285k - 430k;
        int begin = 1;
        ArrayList<Fraction> fractions = new ArrayList<>();


        for (int d = limit; d > begin ; d--) {
            for (int n = begin; n < d; n++) {

                Fraction fraction = Fraction.getFraction(n, d).reduce();

                if ( fractions.isEmpty())
                    fractions.add(fraction);

                //System.out.println(fractions);

                int index = 0;

                if (!fractions.contains(fraction)) {
                    
                    for (int i = 0; i < fractions.size(); i++) { 
                        if (maxFraction(fractions.get(i), fraction) == fraction)
                            index = i + 1;
                        if (index == fractions.size() - 1) {
                            fractions.add(fraction);
                            break;
                        }

                     }           
                    
                }

                if (index != fractions.size() - 1 && !fractions.contains(fraction))
                    fractions.add(index, fraction);
            }

            /*if (fractions.contains(Fraction.getFraction(3,7)))
                break;*/

        }
        System.out.println(fractions.get(fractions.indexOf(Fraction.getFraction(3,7)) - 1 ).getNumerator());
        System.out.println(fractions.size());
    }

    // Get f of the two fractions
    static Fraction maxFraction(Fraction first, Fraction sec) {
        // Declare nume1 and nume2 for get the value of
        // first numerator and second numerator
        int a = first.getNumerator();
        int b = first.getDenominator();
        int c = sec.getNumerator();
        int d = sec.getDenominator();

        // Compute ad-bc
        int Y = a * d - b * c;

        return (Y > 0) ? first : sec;
    }

    

}