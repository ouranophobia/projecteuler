package Solved_Problems;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/*
 * By starting at the top of the triangle below and moving to adjacent
 * numbers on the row below, the maximum total from top to bottom is 23.
 * 
 *    3 
 *   7 4 
 *  2 4 6 
 * 8 5 9 3
 * 
 * That is, 3 + 7 + 4 + 9 = 23.
 * 
 * Find the maximum total from top to bottom in triangle.txt (right click and
 * 'Save Link/Target As...'), a 15K text file containing a triangle with
 * one-hundred rows.
 * 
 */

 public class Problem_067_MaximumPathSumII
 {

    public static void main (String[] args) throws Exception
    {   
        // Reads triangle txt
        File file = new File("D:\\Antonio\\Documents\\GitLab\\ProjectEuler-Java\\txt\\p067_triangle.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));

        // Initialize 2D-Arraylist
        ArrayList<ArrayList<Integer>> triangle = new ArrayList<ArrayList<Integer>>();

        // Adding rows to Arraylist 
        for (int i = 0; i < 100; i++) {
            // Splitting rows at white spaces into String array
            String row = br.readLine();
            String [] temp = row.split(" ");

            // Second Arraylist to be added into the first Arraylist
            // As Integer so Math can be performed
            ArrayList<Integer> rows = new ArrayList<>();

            // Parsing rows as integers
            for (String s : temp)
                rows.add(Integer.parseInt(s));     

            // Adds Integer Arraylist    
            triangle.add(rows);
        }

        // Converting from 2D-Arraylist to 2D-Array
        Integer[][] triangleMaxSum = new Integer[triangle.size()][];
        for (int i = 0; i < triangle.size(); i++) {
        ArrayList<Integer> row = triangle.get(i);
        triangleMaxSum[i] = row.toArray(new Integer[row.size()]);
        }

        // Bottom-Top algorithm to find max sum of triangle
        for (int x = 99; x >= 0 ; x--) {
            for (int y = 0; y < triangleMaxSum[x].length - 1; y++) {
                triangleMaxSum[x-1][y] += Math.max(triangleMaxSum[x][y], triangleMaxSum[x][y+1]);
            }
        }
        
        br.close();

        // Prints max sum of triangle
        System.out.println(triangleMaxSum[0][0]);
    }

 }