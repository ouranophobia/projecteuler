package Solved_Problems;

/*

Each character on a computer is assigned a unique code and the preferred standard is ASCII (American Standard Code for Information Interchange). For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.

A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value, taken from a secret key. The advantage with the XOR function is that using the same encryption key on the cipher text, restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.

For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random bytes. The user would keep the encrypted message and the encryption key in different locations, and without both "halves", it is impossible to decrypt the message.

Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key. If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message. The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.

Your task has been made easy, as the encryption key consists of three lower case characters. Using p059_cipher.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, and the knowledge that the plain text must contain common English words, decrypt the message and find the sum of the ASCII values in the original text.

 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
public class Problem_059_XORDecryption
{
    public static void main(String[] args) throws Exception
    {
        // Read cipher file
        File file = new File("D:\\Antonio\\Documents\\GitLab\\ProjectEuler-Java\\txt\\p059_cipher.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line = br.readLine(); br.close();

        // Initializing Arrays
        String[] lines;
        ArrayList<Integer> num = new ArrayList<>();

        // Splitting commas from cipher and adding to array
        lines = line.split(",");
        for (int i = 0; i < lines.length; i++)
            lines[i] = lines[i].trim(); 

        // Array to List
        ArrayList<String> cipher = new ArrayList<>(Arrays.asList(lines));

        // Adding converted String numberic values to Integer values
        for (String s : cipher)
            num.add(Integer.parseInt(s));   

        // Will contain deciphered text    
        StringBuilder str = new StringBuilder();

        // Defines keyword to search for in deciphered text
        String x = "the";

        // Initializing int variables
        int sum = 0;
        int ascii1 = 0;
        int ascii2 = 0;
        int ascii3 = 0;

        // For loop to check every lowered-case three-letter combination
        for (int i = 97; i < 123; i++)
            for (int l = 97; l < 123; l++)
                for (int k = 97; k < 123; k++){
                    ascii1 = i;
                    ascii2 = l;
                    ascii3 = k;

            // Loop to XOR ASCII values of variables
            for (int j = 0; j <= num.size() - 2; j += 3) {
                str.append(Character.toString((char) (num.get(j + 0) ^ ascii1)));
                str.append(Character.toString((char) (num.get(j + 1) ^ ascii2)));
                str.append(Character.toString((char) (num.get(j + 2) ^ ascii3)));

                // Appends last character where incremetation is outside of conditions and out of bound
                if(j == num.size() - 4 )
                    str.append(Character.toString((char) (num.get(j + 3) ^ ascii1)));
            }
            
            // Array of unwanted characters
            String [] wrong = {"@", "#", "%", "^", "*"};
            boolean check = true;

            // Checks if unwanted character is found in deciphered text
            for(String w : wrong)
                if(str.toString().contains(w)){
                    check = false;
                    break;
                }
            // Prints decipher text without unwanted characters and with keyword    
            if (str.toString().contains(x) && check){
                System.out.println(ascii1 + " " + ascii2 + " " + ascii3);
                System.out.println("Cipher key is '" + (char) ascii1 + "" + (char) ascii2 + "" + (char) ascii3 + "'");
                System.out.println(str.toString());
                
                // Sums the ascii value of the decipher text
                for (char c : str.toString().toCharArray())
                    sum += (int) c;
                System.out.println(sum);    
                break;
            }
            
            // Reset StringBuilder
            str = new StringBuilder();
            
            }
    }
}
