package Solved_Problems;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/*
*
* A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.
*
* The text file, keylog.txt, contains fifty successful login attempts.
*
* Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
*
*/

public class Problem_079_PasscodeDerivation
{
    public static void main (String[] args) throws Exception
    {
        // Scanner to read keylog file
        File file = new File("D:\\Antonio\\Documents\\GitLab\\ProjectEuler-Java\\txt\\p079_keylog.txt");
        Scanner scnr = new Scanner(new FileReader(file));
        
        // Initialize arrays
        ArrayList<String> keyLog = new ArrayList<>();
        ArrayList<Integer> passCode = new ArrayList<>();

        // Adds keylog to arraylist
        while (scnr.hasNextLine())
            keyLog.add(scnr.nextLine()); scnr.close();

        // Algorithm to find passcode    
        for (String s : keyLog) {
            // Adds only known numbers from keylog
            if (!passCode.contains((int) s.charAt(0)))
                passCode.add((int) s.charAt(0));
            if (!passCode.contains((int) s.charAt(1)))
                passCode.add((int) s.charAt(1));
            if (!passCode.contains((int) s.charAt(2)))
                passCode.add((int) s.charAt(2));

            // Keeps track of index of the first digit in passcode arraylist     
            int previousIndex = passCode.indexOf((int) s.charAt(0));
            // Index of new position for the first digit in passcode arraylist
            int index = passCode.indexOf((int) s.charAt(1));
                
            // Proceeds with deletion and new placement of digit only if
            // the index is smaller than previous index
            if (previousIndex >= index){
                passCode.remove(passCode.indexOf((int) s.charAt(0)));
                passCode.add(index, (int) s.charAt(0));
                }

            // Repeats for the second digit    
            previousIndex = passCode.indexOf((int) s.charAt(1));
            index = passCode.indexOf((int) s.charAt(2));

            if (previousIndex >= index){
                passCode.remove(passCode.indexOf((int) s.charAt(1)));
                passCode.add(index, (int) s.charAt(1));
                }
        }

        // Prints passcode
        for (int i : passCode)
            System.out.print((char) i);
    }
}
